---
title: Aquil Rodrigues
linkcolor: blue
pdf-engine: xelatex
header-includes:
  - \usepackage[margin=.5in]{geometry}
classoption: "12pt"
---

# About

I'm Aquil Rodrigues.
An Electrical Engineer who would seek a challenging career to improve my skills and expand learning. To apply creativity and innovation to all the works handed.

## Contact

- Email: [aquilrds@gmail.com](mailto:aquilrds@gmail.com)
- Phone: 8762920284

## Skills

### Technical Skills

#### Languages

- C, Python, C++, Dart

#### Frameworks Used

- Flutter, LVGL

#### RTOS Used

- MbedOS, FreeRTOS

#### Version Controls

- Git

#### Technologies Used

- Bluetooth Low Energy 5.3
- Low power touch screen HMI's

#### Additional Info

- Good understanding of schmeatics
- Good at hardware and software debugging

## Institutions

- B.Tech., BMS College of Engineering under VTU Board 2017--2021.
  - Secured CGPA of 9.12
- PUC., Poorna Prajna PU College 2015--2017
  - Passed under State Board with 91%
- Little Rock Indian School 2015
  - Completed 10th with 9.6 CGPA

## My Works

### Python based automated test for low power Burner HMI's

Understanding custom HMI protocol and simulating the packets through serial port. Testing the display with the custom packets to enusure end to end test.
Prepared a python package which can be installed in PC to test 7 segment custom display, stress test.

### Touch Screen HMI for Boilers

Was involved in development and testing of touch screen HMI. Developed solution for remote debugging and accessing field device remotely which is not equipped with internet connection.

### Test Bench for Bluetooth Dongle

Prepared an architecture for testing lab developed bluetooth dongle. Simulated activities done by mobile application using python. Integrated hardware testing in development build pipeline which can be triggered using gitlab pipline.

### Smart Bin for College Technical Fest 2019

## Declaration

I hereby declare that the information furnished above is true and correct to best of my knowledge

##### Aquil Rodrigues
