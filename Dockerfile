FROM ubuntu:22.04
ARG DEBIAN_FRONTEND=noninteractive


# Echo commands back to the terminal
RUN set -x \
    && apt -y update \
    && apt -y install \
     wget \
     texlive-xetex \
     texlive-fonts-extra

RUN set -x \
    && wget https://github.com/jgm/pandoc/releases/download/3.1.11.1/pandoc-3.1.11.1-1-amd64.deb \
    && dpkg -i ./pandoc-3.1.11.1-1-amd64.deb
  
RUN set -x \
    && apt-get -y install fonts-noto-cjk
